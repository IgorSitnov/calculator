import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Enter mathematical expression such as  4.2 + 2 * 3 / 3 - 6.1" + "\n" + "To exit enter \"exit\"");
        Scanner sc = new Scanner(System.in);
        String line = "";
        double result = 0;
        outer:
        do {
            System.out.print("-> ");
            line = sc.nextLine();
            line = line.trim();
            try {
                ValidateLine.validate(line);
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                System.out.println("The string must contain a mathematical expression such as  4.2 + 2 * 3 / 3 - 6.1\n");
                continue outer;
            }
            try {
                result = CalculateExpression.calculate(ParseLine.parse(line));
                System.out.print("Result: ");
                System.out.printf("%.4f", result);
                System.out.println("\n");
            } catch (ArithmeticException e) {
                System.out.println("!!! " + e.getMessage() + "\nTry again\n");
            }
        }
        while (!line.equals("exit"));
    }
}
