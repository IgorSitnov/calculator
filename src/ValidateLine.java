public class ValidateLine {
    static boolean validate(String line) {
        String regex = "^(\\d+(\\.{1}\\d+)*\\s*(\\*{1}|\\+{1}|\\-{1}|\\/{1})\\s*)+(s*\\d+(\\.{1}\\d+)*)?";
        if (line.matches(regex)) {
            return true;
        }
        throw new IllegalArgumentException();
    }
}
