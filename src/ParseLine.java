import java.util.Stack;

public class ParseLine {
    static StringBuilder parse(String line) {
        Stack<Character> stack = new Stack();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            char character = line.charAt(i);
            if (priority(character) > 0) {
                if (stack.size() != 0 && priority(stack.peek()) >= priority(character)) {
                    stringBuilder.append(" ");
                    stringBuilder.append(stack.pop());
                }
                stringBuilder.append(" ");
                stack.add(character);
            } else {
                if (!Character.toString(character).equals(" ")) {
                    stringBuilder.append(character);
                }
            }
        }
        while (!stack.isEmpty()) {
            stringBuilder.append(" ");
            stringBuilder.append(stack.pop());
        }
        return stringBuilder;
    }

    static int priority(char character) {
        switch (character) {
            case '-':
                return 1;
            case '+':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            default:
                return 0;
        }
    }
}
