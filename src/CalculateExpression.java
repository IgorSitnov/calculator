import java.util.Stack;

public class CalculateExpression {
    static double calculate(StringBuilder stringBuilder) {
        String[] polishArray = stringBuilder.toString().split(" ");
        Stack<Double> stack = new Stack<>();
        for (int i = 0; i < polishArray.length; i++) {
            if (ParseLine.priority(polishArray[i].charAt(0)) == 0) {
                stack.push(Double.valueOf(polishArray[i]));
            } else {
                double secondNumber = stack.pop();
                double firstNumber = stack.pop();
                switch (polishArray[i]) {
                    case "-":
                        stack.push(firstNumber - secondNumber);
                        break;
                    case "+":
                        stack.push(firstNumber + secondNumber);
                        break;
                    case "*":
                        stack.push((firstNumber * secondNumber));
                        break;
                    case "/":
                        if (secondNumber == 0) {
                            throw new ArithmeticException("Division by 0");
                        }
                        stack.push((firstNumber / secondNumber));
                        break;
                }
            }
        }
        return stack.peek();
    }
}
